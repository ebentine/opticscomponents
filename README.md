# README #

Based on [ComponentLibrary](http://www.gwoptics.org/ComponentLibrary/). I didn't like all of the symbols, and wanted to have something more consistent with our lab's optics.

### How do I get set up? ###

Place the inkscape file into the _share/symbols_ folder in inkscape.